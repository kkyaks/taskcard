from collections import defaultdict
import re

from sockjs.tornado import SockJSConnection

from streaming import ResponseDict

class Connection(SockJSConnection):
    # session collections
    everybody = set()
    users = defaultdict(set)
    organizations = defaultdict(set)
    views = defaultdict(set)

    # to find views
    # in the form of ``'/views/7/streaming/585/w9pyxro5/websocket'``
    url_re = re.compile(r'^(.*)/streaming/[^/.]+/[^/.]+/[^/.]+$')
    # to cache views.view related flask url rules
    url_rules = []

    def _name(self):
        return '{0[full_name]} {0[username]}'.format(self.flask_session['trello'])

    def on_open(self, request):
        # Note: request == self.session.conn_info

        # get secure cookie session from flask
        # cookie in Cookie.Morse. It needed to be converted into dict type;
        # which is accepted by
        # `flask.sessions.SecureCookieSessionInterface.open_session`
        self.cookies = dict([(k, v.value) for k, v in request.cookies.iteritems()])
        flask = Connection.flask
        self.flask_session = flask.session_interface.open_session(flask, self)

        self.broadcast(self.everybody, '{0} has joined.'.format(self._name()))

        # How about using multiplex?
        # https://github.com/mrjoes/sockjs-tornado/blob/master/examples/multiplex/multiplex.py
        self.everybody.add(self)

        # collection per org
        for id_ in self.flask_session['trello']['org_ids']:
            self.organizations[id_].add(self)

        # per view
        # cache url rules
        if not self.url_rules:
            allow = ('views.view', 'views.all_cards',)
            for r in flask.url_map.iter_rules():
                if 'GET' in r.methods and  r.endpoint in allow:
                    self.url_rules.append(r)

        # Find where you are now from requested uri
        self.current_view = None
        url = self.url_re.match(self.session.handler.request.uri).group(1)
        for r in self.url_rules:
            match = r.match('|'+url) # ``"subdomain|/path(method)"``
            if match is not None:
                self.current_view = match.get('id', '.') if match else 'all-cards'
                self.views[self.current_view].add(self)
                break

        # collection per user
        me = self.users[self.flask_session['trello']['id']]
        me.add(self)

        self.broadcast(me, ResponseDict('me', 'sync', data=self.flask_session['trello']))

        Connection.observer.add_event_listener(self)

    def on_message(self, message):
        ''' message from client via sock.js '''
        self.broadcast(self.everybody, message)

    def on_mq_message(self, channel, method, header, body):
        ''' message from mq '''
        self.broadcast(self.everybody, body)

    def on_close(self):
        self.everybody.remove(self)
        try:
            self.users[self.flask_session['trello']['username']].remove(self)
        except KeyError:
            pass

        for id_ in self.flask_session['trello']['org_ids']:
            try:
                org = self.organizations[id_]
                org.remove(self)
            except KeyError:
                pass

        try:
            view = self.views[self.current_view]
            view.remove(self)
        except KeyError:
            pass

        self.broadcast(self.everybody, '{0} has left.'.format(self._name()))

        Connection.observer.remove_event_listener(self)

import json

'''
class Response(object):
    def __init__(self, type, action=None, id=None, data=None, **kwargs):
        res = { 'type': type }

        if action:
            res['action'] = action
        if id:
            res['id'] = id
        if data:
            res['data'] = data

        res.update(kwargs)

        self.res = res

    def __json__(self):
        return json.dumps(self.res, indent=None)

    def __str__(self):
        return self.__json__()
'''

def ResponseDict(type, action, data, org=None, user=None, session=None, **kwargs):
    res = { 'type': type, 'action': action, 'data': data }

    if 'id' in data:
        res['id'] = data['id']

    res.update(kwargs)
    return res

def Response(type, action, data, org=None, user=None, session=None, **kwargs):
    return json.dumps(ResponseDict(type, action, data, org=None, user=None, 
                                   session=None, **kwargs), indent=None)

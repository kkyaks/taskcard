import logging
import json

from pika.adapters.tornado_connection import TornadoConnection

import mq

class PikaObserver(object):
    def __init__(self, io_loop):
        logging.info('PikaClient: __init__')
        self.io_loop = io_loop

        self.connected = False
        self.connecting = False
        self.connection = None
        self.channel = None

        self.event_listeners = set([])

    def connect(self):
        if self.connecting:
            logging.info('PikaClient: Already connecting to RabbitMQ')
            return

        logging.info('PikaClient: Connecting to RabbitMQ')
        self.connecting = True

        self.connection = TornadoConnection(mq.get_connection_params(),
                                            on_open_callback=self.on_connected)
        self.connection.add_on_close_callback(self.on_closed)

    def on_connected(self, connection):
        logging.info('PikaClient: connected to RabbitMQ')
        self.connected = True
        self.connection = connection
        self.connection.channel(self.on_channel_open)

    def on_channel_open(self, channel):
        # declare exchanges, which in turn, declare
        # queues, and bind exchange to queues
        logging.info('PikaClient: Channel open, Declaring exchange')
        self.channel = channel
        self.channel.queue_declare(callback=self.on_queue_declared,
                                   **mq.queues['streaming'])

    def on_queue_declared(self, frame):
        """Called when RabbitMQ has told us our Queue has been declared,
           frame is the response from RabbitMQ"""
        self.channel.basic_consume(self.on_message, queue='streaming')

    def on_closed(self, connection):
        logging.info('PikaClient: rabbit connection closed')
        self.io_loop.stop()

    def on_message(self, channel, method, header, body):
        logging.info('PikaClient: message received: %s' % body)
        #self.notify_listeners(body)
        for listener in self.event_listeners:
            #listener.on_mq_message(channel, method, header, body)
            listener.session.send_message(json.loads(body))

    '''
    def notify_listeners(self, body):
        # here we assume the message the sourcing app

        for listener in self.event_listeners:
            listener.session.send_message(body)
            logging.info('PikaClient: notified %s' % repr(listener))
    '''

    def add_event_listener(self, listener):
        self.event_listeners.add(listener)
        logging.info('PikaClient: listener %s added' % repr(listener))

    def remove_event_listener(self, listener):
        try:
            self.event_listeners.remove(listener)
            logging.info('PikaClient: listener %s removed' % repr(listener))
        except KeyError:
            pass

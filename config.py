DEBUG = True

# Flask related

SECRET_KEY = 'somethingveryhardtoguess'
CSRF_ENABLED = True
CSRF_SESSION_KEY = 'somethingveryhardtoguess'

# Trello API related
TRELLO_API_KEY = '<api key>'
TRELLO_API_SECRET = '<secret>'

# Rabbit MQ related

MQ_USERNAME = 'guest'
MQ_PASSWORD = 'guest'

MQ_HOST = 'localhost'
MQ_PORT = 5672
MQ_VIRTUAL_HOST = '/'

# See http://docs.fabfile.org/en/latest/usage/env.html
FAB_ENV = {}

try:
    from config_local import *
except ImportError:
    pass

from fabric.api import cd, env, hide, local, puts, prefix, run, settings, sudo
from contextlib import contextmanager as _contextmanager

from config import FAB_ENV
env.update(FAB_ENV)

@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield

def setup_rabbitmq():
    package = 'deb http://www.rabbitmq.com/debian/ testing main'
    path = '/etc/apt/sources.list'
    key_id = 'RabbitMQ Release Signing Key <info@rabbitmq.com>'

    with settings(hide('warnings', 'running', 'stdout', 'stderr'),
                  warn_only=True):
        check = run('grep "{0}" {1}'.format(package, path))

    if not check:
        puts('Setting up RabbitMQ APT repository')
        sudo('echo "{0}" >> {1}'.format(package, path))

    with settings(hide('warnings', 'running', 'stdout', 'stderr'),
                  warn_only=True):
        check = key_id in sudo('apt-key list')

    if not check:
        local('wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc')
        local('sudo apt-key add rabbitmq-signing-key-public.asc')
        local('rm rabbitmq-signing-key-public.asc')

    with settings(hide('stdout')):
        sudo('apt-get update')

    sudo('apt-get install rabbitmq-server')

def setup():
    setup_rabbitmq()

def run_worker_server():
    with virtualenv():
        run('celery  worker --loglevel=info')

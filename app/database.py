from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///./test.db', convert_unicode=True, echo=True)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

'''
``Card._after_insert`` is triggered before the actual commit,
so updated ``Card.tag_values`` cannot be retrieve.

There must be a good solution for this. Here is a hack for the time being.
You have to manually trigger events later.
'''

class SuppressEvents(object):
    def __enter__(self):
        self.connection = db_session.connection()
        self.connection.suppress_events = True

    def __exit__(self, type, value, tb):
        self.connection.suppress_events = False

db_session.suppress_events = SuppressEvents

Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
    import tags.models
    import cards.models
    import views.models
    Base.metadata.create_all(bind=engine)

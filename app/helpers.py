from sqlalchemy.orm.exc import NoResultFound
from flask import abort

def get_or_404(cls, *args, **kwargs):
    ''' A shortcut to find one object or call abort(404)
        :param cls: sqlalchemy orm model class
        :param *args: args
    '''
    try:
        return cls.query.filter(*args).filter_by(**kwargs).one()
    except NoResultFound:
        abort(404)

def get_or_create(cls, **kwargs):
    ''' A shortcut to find one object with the kwargs or
        create a new object with kwargs set
        :param cls: sqlalchemy orm model class
        :param **kwargs: kwargs
    '''
    if kwargs is None:
        raise ValueError('Empty kwargs')

    try:
        return (False, cls.query.filter_by(**kwargs).one())
    except NoResultFound:
        obj = cls()

        for k, v in kwargs.iteritems():
            setattr(obj, k, v)

        return (True, obj)

''' `Collection.Counter` class for Python 2.5 and 2.6
'''
from operator import itemgetter
from heapq import nlargest
from itertools import repeat, ifilter

class _Counter(dict):
    '''Dict subclass for counting hashable objects.  Sometimes called a bag
    or multiset.  Elements are stored as dictionary keys and their counts
    are stored as dictionary values.

    >>> _Counter('zyzygy')
    _Counter({'y': 3, 'z': 2, 'g': 1})

    '''

    def __init__(self, iterable=None, **kwds):
        '''Create a new, empty _Counter object.  And if given, count elements
        from an input iterable.  Or, initialize the count from another mapping
        of elements to their counts.

        >>> c = _Counter()                           # a new, empty counter
        >>> c = _Counter('gallahad')                 # a new counter from an iterable
        >>> c = _Counter({'a': 4, 'b': 2})           # a new counter from a mapping
        >>> c = _Counter(a=4, b=2)                   # a new counter from keyword args

        '''        
        self.update(iterable, **kwds)

    def __missing__(self, key):
        return 0

    def most_common(self, n=None):
        '''List the n most common elements and their counts from the most
        common to the least.  If n is None, then list all element counts.

        >>> _Counter('abracadabra').most_common(3)
        [('a', 5), ('r', 2), ('b', 2)]

        '''        
        if n is None:
            return sorted(self.iteritems(), key=itemgetter(1), reverse=True)
        return nlargest(n, self.iteritems(), key=itemgetter(1))

    def elements(self):
        '''Iterator over elements repeating each as many times as its count.

        >>> c = _Counter('ABCABC')
        >>> sorted(c.elements())
        ['A', 'A', 'B', 'B', 'C', 'C']

        If an element's count has been set to zero or is a negative number,
        elements() will ignore it.

        '''
        for elem, count in self.iteritems():
            for _ in repeat(None, count):
                yield elem

    # Override dict methods where the meaning changes for _Counter objects.

    @classmethod
    def fromkeys(cls, iterable, v=None):
        raise NotImplementedError(
            '_Counter.fromkeys() is undefined.  Use _Counter(iterable) instead.')

    def update(self, iterable=None, **kwds):
        '''Like dict.update() but add counts instead of replacing them.

        Source can be an iterable, a dictionary, or another _Counter instance.

        >>> c = _Counter('which')
        >>> c.update('witch')           # add elements from another iterable
        >>> d = _Counter('watch')
        >>> c.update(d)                 # add elements from another counter
        >>> c['h']                      # four 'h' in which, witch, and watch
        4

        '''        
        if iterable is not None:
            if hasattr(iterable, 'iteritems'):
                if self:
                    self_get = self.get
                    for elem, count in iterable.iteritems():
                        self[elem] = self_get(elem, 0) + count
                else:
                    dict.update(self, iterable) # fast path when counter is empty
            else:
                self_get = self.get
                for elem in iterable:
                    self[elem] = self_get(elem, 0) + 1
        if kwds:
            self.update(kwds)

    def copy(self):
        'Like dict.copy() but returns a _Counter instance instead of a dict.'
        return _Counter(self)

    def __delitem__(self, elem):
        'Like dict.__delitem__() but does not raise KeyError for missing values.'
        if elem in self:
            dict.__delitem__(self, elem)

    def __repr__(self):
        if not self:
            return '%s()' % self.__class__.__name__
        items = ', '.join(map('%r: %r'.__mod__, self.most_common()))
        return '%s({%s})' % (self.__class__.__name__, items)

    # Multiset-style mathematical operations discussed in:
    #       Knuth TAOCP Volume II section 4.6.3 exercise 19
    #       and at http://en.wikipedia.org/wiki/Multiset
    #
    # Outputs guaranteed to only include positive counts.
    #
    # To strip negative and zero counts, add-in an empty counter:
    #       c += _Counter()

    def __add__(self, other):
        '''Add counts from two counters.

        >>> _Counter('abbb') + _Counter('bcc')
        _Counter({'b': 4, 'c': 2, 'a': 1})


        '''
        if not isinstance(other, _Counter):
            return NotImplemented
        result = _Counter()
        for elem in set(self) | set(other):
            newcount = self[elem] + other[elem]
            if newcount > 0:
                result[elem] = newcount
        return result

    def __sub__(self, other):
        ''' Subtract count, but keep only results with positive counts.

        >>> _Counter('abbbc') - _Counter('bccd')
        _Counter({'b': 2, 'a': 1})

        '''
        if not isinstance(other, _Counter):
            return NotImplemented
        result = _Counter()
        for elem in set(self) | set(other):
            newcount = self[elem] - other[elem]
            if newcount > 0:
                result[elem] = newcount
        return result

    def __or__(self, other):
        '''Union is the maximum of value in either of the input counters.

        >>> _Counter('abbb') | _Counter('bcc')
        _Counter({'b': 3, 'c': 2, 'a': 1})

        '''
        if not isinstance(other, _Counter):
            return NotImplemented
        _max = max
        result = _Counter()
        for elem in set(self) | set(other):
            newcount = _max(self[elem], other[elem])
            if newcount > 0:
                result[elem] = newcount
        return result

    def __and__(self, other):
        ''' Intersection is the minimum of corresponding counts.

        >>> _Counter('abbb') & _Counter('bcc')
        _Counter({'b': 1})

        '''
        if not isinstance(other, _Counter):
            return NotImplemented
        _min = min
        result = _Counter()
        if len(self) < len(other):
            self, other = other, self
        for elem in ifilter(self.__contains__, other):
            newcount = _min(self[elem], other[elem])
            if newcount > 0:
                result[elem] = newcount
        return result

try:
    from collections import Counter
except ImportError:
    Counter = _Counter

class HTMLString(str):
    def __html__(self):
            return self

from cgi import escape
def html_params(**kwargs):
    """
    Generate HTML parameters from inputted keyword arguments.

    The output value is sorted by the passed keys, to provide consistent output
    each time this function is called with the same parameters.  Because of the
    frequent use of the normally reserved keywords `class` and `for`, suffixing
    these with an underscore will allow them to be used.

    >>> html_params(name='text1', id='f', class_='text') == 'class="text" id="f" name="text1"'
    True
    """
    params = []
    for k,v in sorted(kwargs.iteritems()):
        if k in ('class_', 'class__', 'for_'):
            k = k[:-1]
        k = k.replace('__', '-')
        if v is True:
            params.append(k)
        else:
            params.append('%s="%s"' % (str(k), escape(str(v), quote=True)))
    return ' '.join(params)

class RadioListWidget(object):
    """ Renders a list of radio/checkbox fields in Bootstrap markup """
    def __init__(self, prefix_label=False):
        self.prefix_label = prefix_label

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        html = ['<div %s>' % html_params(**kwargs)]
        for subfield in field:
            if self.prefix_label:
                html.append('<label class="radio">%s: %s</label>' % (subfield.label.text, subfield()))
            else:
                html.append('<label class="radio">%s %s</label>' % (subfield(), subfield.label.text))
        html.append('</div>')
        return HTMLString(''.join(html))

from flask import json, Response, request
from flask import jsonify as jsonify_orig
def jsonify(*args, **kwargs):
    ''' Expand flask's original jsonify function to accept list params '''
    if not kwargs and args:
        return Response(json.dumps(*args,
                                   indent=None if request.is_xhr else 2),
                        mimetype='application/json')
    else:
        return jsonify_orig(*args, **kwargs)

from functools import wraps
def public(f):
    ''' A view decorator to indicate a non-login-required view '''
    @wraps(f)
    def wrapper(*args, **kwargs):
        return f(*args, **kwargs)

    wrapper.is_public = True

    return wrapper

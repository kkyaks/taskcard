from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, backref
from sqlalchemy.schema import UniqueConstraint

from ..database import Base

class Tag(Base):
    '''Tag is a something...

    :param id: the primary key
    :type id: int
    :param name: name of the tag
    :type name: str
    :param multiple: this tag can be appeared more than once in a card
    :type name: bool
    :param required: this tag is required in a card
    :type name: bool
    :param read_only: this tag is system generated, and cannot be edited
    :type name: bool

    '''
    __tablename__ = 'tag'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    multiple = Column(Boolean, default=False)
    required = Column(Boolean, default=False)
    read_only = Column(Boolean, default=False)

    values = relationship('TagValue', backref=backref('tag', order_by=id), lazy='dynamic')

    __mapper_args__ = {
        'order_by': 'name',
    }

    def __json__(self):
        return {'id': self.id, 'value': self.name }

    def __str__(self):
        return '{0}'.format(self.name)

class TagValue(Base):
    '''TagValue is set of Tag and its values

    :param id: the primary key
    :type id: int
    :param tag_id: foreign key to :class:`tags.models.Tag`
    :type tag_id: int
    :param value: value of the tag
    :type name: str

    The (Tag, Value) pair has to be unique.
    '''
    __tablename__ = 'tag_value'

    id = Column(Integer, primary_key=True)
    tag_id = Column(Integer, ForeignKey('tag.id'), nullable=False)
    value = Column(String)
    trello_id = Column(String, nullable=True, default=None)

    __table_args__ = (UniqueConstraint('tag_id', 'value', 'trello_id'), )
    __mapper_args__ = {
        'order_by': ['tag_id', 'value'],
    }

    def __str__(self):
        return '{0.tag.name}: {0.value}'.format(self)

    def __json__(self):
        return {'id': self.id, 'tag': self.tag.name, 'value': self.value,
                'tag_id': self.tag_id, 'tag_value': str(self)}

    def tree_data(self):
        ''' Returns a dict which represent the following
        {
            "data" : "node_title",
            // omit `attr` if not needed; the `attr` object gets passed to the jQuery `attr` function
            "attr" : { "id" : "node_identificator", "some-other-attribute" : "attribute_value" },
            // `state` and `children` are only used for NON-leaf nodes
            "state" : "closed", // or "open", defaults to "closed"
            "children" : [ /* an array of child nodes objects */ ]
        }
        {
            // `data` can also be an object
            "data" : { 
                "title" : "The node title", 
                // omit when not needed
                "attr" : {}, 
                // if `icon` contains a slash / it is treated as a file, used for background
                // otherwise - it is added as a class to the <ins> node
                "icon" : "folder"
            },

            // the `metadata` property will be saved using the jQuery `data` function on the `li` node
            "metadata" : "a string, array, object, etc",

            // if you use the language plugin - just set this property
            // also make sure that `data` is an array of objects
            "language" : "en" // any code you are using
        }

        '''
        return dict(data=str(self), state='closed',
                    attr={'data-id': self.id, 'data-tag-id': self.tag_id,
                          'data-tag': self.tag.name, 'data-value': self.value})

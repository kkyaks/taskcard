from flask import Blueprint, abort
from flask import flash, redirect, render_template, request, url_for
from sqlalchemy.exc import IntegrityError

import config
from . import forms
from .models import Tag, TagValue

from ..database import db_session
from ..helpers import get_or_404

mod = Blueprint('tags', __name__, url_prefix='/tags')

@mod.route('/')
def index():
    return render_template('tags/index.html', tags=Tag.query)

@mod.route('/new', methods=['GET', 'POST'])
def new():
    return edit()

@mod.route('/<int:tag>/edit', methods=['GET', 'POST'])
def edit(tag=None):
    is_add = tag is None

    if is_add:
        tag = Tag()
        form = forms.TagForm(request.form)
        template = 'tags/new.html'
    else:
        tag = get_or_404(Tag, Tag.id==tag)
        # this can be rewritten as tag = get_or_404(Tag, Tag.id==tag, Tag.read_only==False)
        if tag.read_only:
            abort(404)
        form = forms.TagForm(request.form, tag)
        template = 'tags/edit.html'

    if form.validate_on_submit():
        form.populate_obj(tag)
        try:
            if is_add:
                db_session.add(tag)
            db_session.commit()
            return redirect(url_for('tags.index'))

        except IntegrityError:
            db_session.rollback()

    return render_template(template, form=form, tag=tag)

@mod.route('/<int:tag>/values')
def values(tag):
    tag = get_or_404(Tag, Tag.id==tag)

    return render_template('tags/index.html', tags=Tag.query, tag=tag)

@mod.route('/<int:tag>/values/<int:value>/edit', methods=['GET', 'POST'])
def edit_value(tag, value=None):
    return new_value(tag, value)

@mod.route('/<int:tag>/values/new', methods=['GET', 'POST'])
def new_value(tag, value=None):
    tag = get_or_404(Tag, Tag.id==tag)
    # this can be rewritten as tag = get_or_404(Tag, Tag.id==tag, Tag.read_only==False)
    if tag.read_only and not config.DEBUG:
        abort(404)

    is_add = value is None

    if is_add:
        tagvalue = TagValue(tag=tag)
        form = forms.TagValueForm(request.form)
        template = 'tags/new-value.html'
    else:
        if config.DEBUG:
            tagvalue = get_or_404(TagValue, Tag.id==tag.id, TagValue.id==value)
        else:
            tagvalue = get_or_404(TagValue, Tag.read_only==False, Tag.id==tag.id,
                                            TagValue.id==value)
        form = forms.TagValueForm(request.form, tagvalue)
        template = 'tags/edit-value.html'

    if form.validate_on_submit():
        form.populate_obj(tagvalue)
        try:
            if is_add:
                db_session.add(tagvalue)

            db_session.commit()
            return redirect(url_for('tags.values', tag=tag.id))

        except IntegrityError:
            db_session.rollback()
            flash('Value "{0}" for {1} already exists.'.format(form.value.data, tag.name), 'error')

    return render_template(template, form=form, tag=tag, tagvalue=tagvalue)

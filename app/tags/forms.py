from flask.ext import wtf
from flask.ext.wtf import validators

class TagForm(wtf.Form):
    ''' A form which represents :class:`tags.models.Tag`
    '''
    name = wtf.TextField('Tag', validators=[validators.Required(), validators.DataRequired()])
    required = wtf.BooleanField('Required')
    multiple = wtf.BooleanField('Multiple')

class TagValueForm(wtf.Form):
    ''' A form which represents :class:`tags.models.TagValue`
    '''
    value = wtf.TextField('Value', validators=[validators.Required(), validators.DataRequired()])


from flask.ext import wtf
from flask.ext.wtf import validators

from ..tags.models import TagValue, Tag

from ..helpers import Counter

def get_tags():
    return TagValue.query.all()

def tag_value_validator(form, field):
    tag_ids  = [ v.tag_id for v in field.data ]

    required = Tag.query.filter(Tag.required == True).values(Tag.id)
    check = dict([ (r[0], r[0] in tag_ids) for r in required])

    if False in check.values():
        ids = [ k for k, v in check.iteritems() if not v ]
        tag_names = [ t[0] for t in Tag.query.filter(Tag.id.in_(ids)).values(Tag.name)]
        raise validators.ValidationError('You need to have at least of the following tags : {0}'.format(', '.join(tag_names)))

    singles = [ c[0] for c in Tag.query.filter(Tag.multiple == False).values(Tag.id) ]
    counts = Counter([ c for c in tag_ids if c in singles ])
    more_than_one = [ id for id, count in counts.iteritems() if count > 1]

    if more_than_one:
        tag_names = [ t[0] for t in Tag.query.filter(Tag.id.in_(more_than_one)).values(Tag.name)]
        raise validators.ValidationError('You are allow to have at most one of the following tags : {0}'.format(', '.join(tag_names)))

class CardForm(wtf.Form):
    ''' A form which represents :class:`cards.models.Card`
    '''
    name = wtf.TextField('Name', validators=[validators.Required(), validators.DataRequired()])
    description = wtf.TextAreaField('Description')
    is_complete = wtf.BooleanField('Completed')

    planned_start  = wtf.DateField('Planned Start', validators=[validators.Optional()])
    planned_finish = wtf.DateField('Planned Finish', validators=[validators.Optional()])
    planned_hours = wtf.DecimalField('Planned Hour', validators=[validators.Optional(), validators.NumberRange(0)], default=0)

    actual_start  = wtf.DateField('Actual Start', validators=[validators.Optional()])
    actual_finish = wtf.DateField('Actual Finish', validators=[validators.Optional()])
    actual_hours = wtf.DecimalField('Actual Hour', validators=[validators.Optional(), validators.NumberRange(0)], default=0)

    tag_values = wtf.QuerySelectMultipleField('Tag Values', query_factory=get_tags, validators=[tag_value_validator])

import celery
from flask import Blueprint, abort
from flask import flash, redirect, render_template, request, session, url_for
from formencode import validators
from sqlalchemy.orm.exc import NoResultFound
import trello
from werkzeug.datastructures import ImmutableMultiDict

import config
import tasks

from . import forms
from .models import Card
from ..tags.models import Tag, TagValue

from ..helpers import get_or_404, get_or_create, jsonify
from ..database import db_session

mod = Blueprint('cards', __name__, url_prefix='/cards')

@mod.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST': # ajax request for new post
        return edit(None)
    return redirect(url_for('views.all_cards'))

@mod.route('/new')
def new(id=None):
    if id is None:
        card = Card()
    else:
        card = get_or_404(Card, Card.id==id)
    return render_template('cards/new.html', card=card)

@mod.route('/<int:id>')
def view(id):
    return new(id)

@mod.route('/<int:id>', methods=['POST', 'PUT'])
def edit(id=None):
    is_new = id is None
    ret = dict()

    if request.json:
        json = [(k, v if v else '') for k, v in request.json.iteritems()]

        for t in request.json.get('tags', []) or []:
            json.append(('tag_values', unicode(t)))

        try: # if is_complete checkbox is unchecked, remove it from the list
            json.remove(('is_complete', ''))
        except ValueError:
            pass

        data = ImmutableMultiDict(json)
    else:
        data = request.form

    if is_new:
        card = Card()
        form = forms.CardForm(data, csrf_enabled=False)
    else:
        card = get_or_404(Card, Card.id==id)
        form = forms.CardForm(data, card, csrf_enabled=False)

    if form.validate_on_submit():
        form.populate_obj(card)

        if is_new:
            db_session.add(card)
            db_session.commit()
            ret['created'] = True
        else:
            with db_session.suppress_events():
                db_session.commit()

            card.stream('sync')
            ret['updated'] = True

            if card.trello_id:
                token = session['access_token']['oauth_token']
                tasks.send_changes.delay(token, card.id)

        ret['id'] = card.id
    else:
        ret['error'] = form.errors

    return jsonify(**ret)

@mod.route('/<int:id>', methods=['DELETE'])
def delete(id):
    db_session.delete(get_or_404(Card, Card.id==id))
    db_session.commit()

    return jsonify(id=id, removed=True)

@mod.route('/<int:id>/tags', methods=['POST', 'GET'])
def tags(id):
    card = get_or_404(Card, Card.id==id)
    ret = {}

    def _json(tvs):
        return [tv.__json__() for tv in tvs]

    if request.method == 'POST':
        added = []
        removed = []

        # TODO?: make these as a separate form?
        attach = validators.StringBool().to_python(request.form['attach'])
        tag_value_id = validators.Int().to_python(request.form['id'])
        has_tag = False

        try:
            tv = card.tag_values.filter(TagValue.id == tag_value_id).one()
            has_tag = True
        except NoResultFound:
            tv = get_or_404(TagValue, TagValue.id == tag_value_id)

        l = card.items()

        if attach and not has_tag: # add exists tag value
            card.tag_values.append(tv)
            l.append(('tag_values', unicode(tv.id)))
            added.append(tv)
        elif not attach and has_tag: # remove new tag value
            card.tag_values.remove(tv)
            l.remove(('tag_values', unicode(tv.id)))
            removed.append(tv)

        # use `app.cards.forms.CardForm` to check tag requirements
        # (required or singularity)
        form = forms.CardForm(ImmutableMultiDict(l), csrf_enabled=False)

        if not form.validate():
            db_session.rollback()
            abort(400, form.errors)

        with db_session.suppress_events():
            db_session.commit()

        card.stream('sync')
        ret['added'] = _json(added)
        ret['removed'] = _json(removed)

    ret['tags'] = _json(card.tag_values)

    return jsonify(**ret)

@mod.route('/<int:id>/check_for_change', methods=['POST'])
def check_for_change(id):
    card = get_or_404(Card, Card.id==id)
    if card.trello_id:
        token = session['access_token']['oauth_token']
        tasks.check_for_change.delay(token, id)
    return jsonify(checking=True)

# a trello diction which stores (id, type)
# NOTE: cache should be managed per organization
_trello_cache =  {}
_trello_type = { 'organization': 1, 'board': 2, 'list': 4, 'card': 8 }
# stores value: key dict for lookup
_trello_type_vk = dict([(v, k) for k, v in _trello_type.iteritems()])

@mod.route('/import', methods=['POST', 'GET'])
def import_():
    token = session['access_token']['oauth_token']

    if request.method == 'POST':
        items = request.form.getlist('items')

        if not items:
            flash('Please check at least one item', 'error')
            return redirect(url_for('.import_'))

        for i in items:
            if i not in _trello_cache:
                flash('Invalid item', 'error')
                return redirect(url_for('.import_'))

        # probably good idea to move this to somewhere else...
        # Maybe on new Org?
        _, board_tag = get_or_create(Tag, name='Board', read_only=True)
        _, list_tag = get_or_create(Tag, name='List', read_only=True)
        db_session.add_all([board_tag, list_tag])
        db_session.commit()

        _task = tasks.import_cards.subtask
        flash('Scheduled importing...')
        g = celery.group(_task((token, i, _trello_type_vk[_trello_cache[i]])) for i in items)
        g.apply_async()

        return redirect(url_for('views.all_cards'))

    nodes = []

    client = trello.TrelloClient(config.TRELLO_API_KEY, token)
    orgs = client.fetch_json('/members/me/organizations')
    boards = client.fetch_json('/members/me/boards',
                               query_params={'filter': 'members'})

    for o in orgs:
        d = dict(attr={'id': o['id']},
                 data={'icon': 'organization', 'title': o['displayName']},
                 state='closed')

        if config.DEBUG:
            d['dump'] = o

        nodes.append(d)
        _trello_cache[o['id']] = _trello_type['organization']

    for b in boards:
        if b['idOrganization']:
            continue

        d = dict(attr={'id': b['id']},
                 data={'icon': 'board', 'title': b['name']},
                 state='closed', dump=b)

        if config.DEBUG:
            d['dump'] = b

        nodes.append(d)
        _trello_cache[b['id']] = _trello_type['board']

    return render_template('cards/import.html', nodes=nodes)

@mod.route('/import/child_nodes')
def import_child_nodes():
    id = request.args.get('id', None)

    if id is None or id not in _trello_cache:
        abort(404)

    client = trello.TrelloClient(config.TRELLO_API_KEY,
                                 session['access_token']['oauth_token'])
    _type = _trello_cache[id]

    if _type == _trello_type['organization']:
        url = '/organization/{0}/boards'
        _fetch_type = 'board'
    elif _type == _trello_type['board']:
        url = '/board/{0}/lists'
        _fetch_type = 'list'
    elif _type == _trello_type['list']:
        url = '/list/{0}/cards'
        _fetch_type = 'card'
    else:
        abort(404)

    data = client.fetch_json(url.format(id))
    nodes = []

    for d in data:
        di = dict(attr={'id': d['id']},
                  data={'icon': _fetch_type, 'title':d['name']})

        if _trello_type[_fetch_type] != _trello_type['card']:
            di['state'] = 'closed'

        if config.DEBUG:
            di['dump'] = d

        _trello_cache[d['id']] = _trello_type[_fetch_type]

        nodes.append(di)

    return jsonify(nodes)


import datetime
import decimal

from flask import url_for

from sqlalchemy import Boolean, Text, Float, Integer, String, Date, DateTime
from sqlalchemy import Column, Table, ForeignKey, event
from sqlalchemy.orm import relationship
from sqlalchemy.schema import UniqueConstraint

from streaming import Response
import mq

from ..database import Base
from ..tags.models import TagValue

card_tag_values = Table(
    'card_tag_values', Base.metadata,
    Column('card_id', Integer, ForeignKey('card.id')),
    Column('tag_value_id', Integer, ForeignKey('tag_value.id')),
    UniqueConstraint('card_id', 'tag_value_id')
)

class Card(Base):
    '''Card is what we have...

    :param id: the primary key
    :type id: int
    :param name: name of the card
    :type name: str
    :param description:
    :type description: str
    :param planned_hours:
    :type planned_hours: float
    :param planned_start:
    :type planned_start: `datetime.datetime`
    :param planned_finish:
    :type planned_finish: `datetime.datetime`
    :param actual_hours:
    :type actual_hours: float
    :param actual_start:
    :type actual_start: `datetime.datetime`
    :param actual_finish:
    :type actual_finish: `datetime.datetime`

    '''
    __tablename__ = 'card'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(Text)

    planned_hours = Column(Float(2))
    planned_start = Column(Date)
    planned_finish = Column(Date)

    actual_hours = Column(Float(2))
    actual_start = Column(Date)
    actual_finish = Column(Date)

    is_complete = Column(Boolean, default=False)

    created_at = Column(DateTime, default=datetime.datetime.now)
    tag_values = relationship(TagValue, secondary=card_tag_values,
                              backref='cards', lazy='dynamic')

    trello_id = Column(String, nullable=True, default=None)

    # things to be visible via JSON
    __cols__ = set(['id', 'name', 'description', 'planned_hours',
                    'planned_start', 'planned_finish', 'actual_hours',
                    'actual_start', 'actual_finish', 'is_complete',
                    'created_at', 'trello_id'])

    def tree_data(self):
        ''' Returns a dict which represent the following
        {
            "data" : "node_title",
            // omit `attr` if not needed; the `attr` object gets passed to the jQuery `attr` function
            "attr" : { "id" : "node_identificator", "some-other-attribute" : "attribute_value" },
            // `state` and `children` are only used for NON-leaf nodes
            "state" : "closed", // or "open", defaults to "closed"
            "children" : [ /* an array of child nodes objects */ ]
        }
        {
            // `data` can also be an object
            "data" : { 
                "title" : "The node title", 
                // omit when not needed
                "attr" : {}, 
                // if `icon` contains a slash / it is treated as a file, used for background
                // otherwise - it is added as a class to the <ins> node
                "icon" : "folder"
            },

            // the `metadata` property will be saved using the jQuery `data` function on the `li` node
            "metadata" : "a string, array, object, etc",

            // if you use the language plugin - just set this property
            // also make sure that `data` is an array of objects
            "language" : "en" // any code you are using
        }

        '''
        url = url_for('cards.edit', id=self.id)
        return dict(data={'title': self.name, 'icon': 'card', 'attr': {'href': url}},
                    attr={'data-id': self.id, 'data-href': url})

    def _items(self):
        ''' ignore InstanceState '''
        # perform SQL select again after `db_session.commit`
        # since self.__dict__ might be empty after commit
        self.id
        return [(k, getattr(self, k)) for k in self.__cols__ ]

    def items(self):
        ''' to use in `werkzeug.datastructures.MultiDict` '''
        l = [(k, unicode(v) if v is not None else '') for k, v in self._items()]

        for tv in self.tag_values.all():
            l.append(('tag_values', unicode(tv.id)))

        return l

    def __json__(self):
        def _convert(v):
            if isinstance(v, (datetime.datetime, datetime.date)):
                return unicode(v)
            if isinstance(v, decimal.Decimal):
                return float(v)
            return v

        l = [(k, _convert(v)) for k, v in self._items()]
        l.append(('tags', [ tv.__json__() for tv in self.tag_values.all() ]))

        return dict(l)

    def stream(self, type):
        with mq.BlockingConnection('streaming') as conn:
            conn.basic_publish('', Response('card', type , self.__json__()))

    @classmethod
    def _after_insert(cls, mapper, connection, target):
        if hasattr(connection, 'suppress_events') and connection.suppress_events:
            return
        target.stream('add')

    @classmethod
    def _after_update(cls, mapper, connection, target):
        if hasattr(connection, 'suppress_events') and connection.suppress_events:
            return
        target.stream('sync')

    @classmethod
    def _after_delete(cls, mapper, connection, target):
        if hasattr(connection, 'suppress_events') and connection.suppress_events:
            return
        target.stream('destroy')

event.listen(Card, 'after_insert', Card._after_insert)
event.listen(Card, 'after_update', Card._after_update)
event.listen(Card, 'after_delete', Card._after_delete)

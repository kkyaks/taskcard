import re

from flask import Flask, request
from flask import session, render_template

from .helpers import jsonify
from .database import db_session

app = Flask(__name__)
app.config.from_object('config')

from .views_ import *

@app.context_processor
def inject_into_templates():
    from .cards.forms import CardForm
    from .cards.models import Card
    from .tags.models import TagValue
    from .views.models import View

    _rule_regex = re.compile(r'<.+:(.+)>')
    def get_url_rule(endpoint, mustache=False):
        '''Return the url rule by endpoint
           parameter variable types will be stripped off.
           :param mustache: Alternatively, return in mustache format
           :type mustache: ``bool``
        '''
        return _rule_regex.sub(r'{{\1}}' if bool(mustache) else r'<\1>',
                               app.url_map._rules_by_endpoint[endpoint][0].rule)

    ret = dict(is_authenticated='access_token' in session,
               get_url_rule=get_url_rule,
               debug=app.config.get('DEBUG')
               )

    if ret['is_authenticated']:
        override = dict(all_views=View.query,
                        all_tags=TagValue.query,
                        empty_card=Card(),
                        card_form=CardForm())
        ret.update(override)

    return ret

@app.errorhandler(400)
def bad_request(error):
    return jsonify(**error.description), 400

@app.before_request
def check_auth():
    ''' check for authentication. Executed prior to every request.
        Check for usage of @public decorator
        Maybe it's good idea to make a user model at one point.
    '''
    if 'access_token' not in session and \
            request.endpoint and \
            'static' != request.endpoint and \
            not getattr(app.view_functions[request.endpoint], 'is_public', False):
        return render_template('welcome.html', next=request.endpoint)

@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()

from .tags.views import mod as tags_module
app.register_blueprint(tags_module)

from .cards.views import mod as cards_module
app.register_blueprint(cards_module)

from .views.views import mod as views_module
app.register_blueprint(views_module)

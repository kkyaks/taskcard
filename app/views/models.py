
from formencode.variabledecode import variable_encode

from sqlalchemy import Integer, String, Column, ForeignKey, Enum
from sqlalchemy.orm import relationship

from werkzeug.datastructures import ImmutableMultiDict

from ..database import Base

from ..cards.models import Card
from ..tags.models import Tag, TagValue
from ..filters.models import Filter
from ..filters.forms import FilterForm

class Group(Base):
    __tablename__ = 'view_group'

    id = Column(Integer, primary_key=True)
    view_id = Column(Integer, ForeignKey('view.id', ondelete='cascade'))
    order = Column(Integer, default=0)
    tag_id = Column(Integer, ForeignKey(Tag.id))
    tag = relationship(Tag, backref='groups')

class View(Base):
    __tablename__ = 'view'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    type = Column(Enum('tree', 'list'))
    filters = relationship(Filter, order_by=Filter.order, backref='view', lazy='dynamic')
    groups = relationship(Group, order_by=Group.order, backref='view', lazy='dynamic')

    def _get_tags(self, level, parents=None, node=False, add_untagged=True):
        ''' called from `get_nodes` to get tags, untagged and untagged cards '''
        tags = []
        tag = self.groups[level].tag
        q = tag.values.join(Card.tag_values)

        filter_ = FilterForm(self.get_filter_dict())
        q = filter_.prepare_query(q)

        if parents is None:
            parents = []

        for i, p in enumerate(parents):
            if p == -1:
                t = self.groups[i].tag
                q = q.filter(~Card.tag_values.any(TagValue.tag_id == t.id))
            else:
                q = q.filter(Card.tag_values.any(TagValue.id == p))

        id_prefix = 'tag-{0}-{1}-'.format('-'.join(map(str, parents)), level)

        for tv in q.filter(TagValue.tag_id == tag.id):
            if node:
                d = tv.tree_data()
                d['attr']['data-level'] = level
                d['attr']['id'] = id_prefix + str(tv.id)
                tags.append(d)
            else:
                tags.append(tv)

        if add_untagged and node:
            tags.append(dict(data='{0}: untagged'.format(tag.name),
                        state='closed',
                        attr={'data-id': -1, 'data-tag-id': tag.id,
                              'data-level': level, 'data-tag': tag.name,
                              'data-value': 'untagged', 'id': id_prefix + '0'}))

        if parents and parents[-1] == -1: # if the deepest node is untagged, show untagged cards
            tags += self._get_cards(level, parents)

        return tags

    def _get_cards(self, level, parents=None, node=False):
        ''' called from `get_nodes` to get tagged cards '''

        filter_ = FilterForm(self.get_filter_dict())
        q = filter_.prepare_query(Card.query)

        if parents is None:
            parents = []

        # TODO: for untagged level, next level tag types need to be tagged
        for i, p in enumerate(parents):
            if p == -1:
                tag = self.groups[i].tag
                q = q.filter(~Card.tag_values.any(TagValue.tag_id == tag.id))
            else:
                tv = TagValue.query.get(p)
                q = q.filter(Card.tag_values.any(TagValue.id == tv.id))

        return [ c.tree_data() if node else c for c in q ]

    def get_tags(self, level, parents=None, add_untagged=False, node=False):
        ''' returns tags of the current view at a given level'''
        self.groups[level].tag # make sure this level exists
        return self._get_tags(level, parents, add_untagged=add_untagged, node=node)

    def get_cards(self, level, parents=None):
        self.groups[level].tag # make sure this level exists
        return self._get_cards(level, parents)

    def get_nodes(self, level, parents=None, add_untagged=True):
        ''' returns tree structure of the current view at a given level'''
        try:
            return self.get_tags(level, parents, add_untagged=add_untagged, node=True)
        except IndexError:
            return self._get_cards(level, parents, node=True)

    def get_filter_dict(self):
        ''' return filters in `formencode.variabledecode` structure then
            wrap it around with `werkzeug.datastructures.ImmutableMultiDict`
            so that it mimics as if these are HTTP POST'd '''

        ret = []
        _values = ('value', 'value_start', 'value_finish')

        for f in self.filters:
            d = {'key': f.key, 'condition': f.condition}

            for v in _values:
                if f.values[v]: d[v] = f.values[v]

            ret.append(d)

        return ImmutableMultiDict(variable_encode(ret, prepend='filters',
                                                  add_repetitions=False))

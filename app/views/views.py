from flask import Blueprint, abort
from flask import redirect, render_template, request, url_for

from formencode import validators

from . import forms
from .models import View, Group, Filter

from ..cards.models import Card
from ..filters.forms import FilterForm

from ..database import db_session
from ..helpers import get_or_404, jsonify

mod = Blueprint('views', __name__, url_prefix='/views')

@mod.route('/')
def index():
    views = View.query
    return render_template('views/index.html', views=views)

@mod.route('/<int:id>')
def view(id):
    view = get_or_404(View, View.id==id)

    if view.type == 'tree':
        return view_tree(view)
    elif view.type == 'list':
        return view_list(view)

    return abort(404)

def view_tree(view):
    return render_template('views/tree.html', view=view, order=forms.order)

def view_list(view):
    return render_template('views/list.html', view=view, order=forms.order)

@mod.route('/<int:id>/tree')
def tree(id):
    ''' ajax request from jstree to get nodes at a given level'''
    validator = validators.Int()

    view = get_or_404(View, View.id==id)
    level = validator.to_python(request.args['level'])
    parents = [validator.to_python(p) for p in request.args.getlist('values[]')]
    tags = view.get_nodes(level+1, parents=parents)

    return jsonify(tags)

@mod.route('/all-cards')
def all_cards():
    filter_ = FilterForm(request.args)
    q = filter_.prepare_query(Card.query)
    return render_template('views/all-cards.html', cards=q, filter=filter_)

@mod.route('/new', methods=['GET', 'POST'])
def new(id=None):
    is_new = id is None
    if is_new:
        view = View()
        form = forms.ViewForm(request.form)
    else:
        view = get_or_404(View, View.id==id)
        form = forms.ViewForm(request.form, view)

    filter_ = FilterForm(request.form if request.form else view.get_filter_dict(), allow_empty=True)

    if form.validate_on_submit():
        form.populate_obj(view)

        if not is_new:
            # to make it simple, just remove all groups assigned to this view
            # otherwise, call views.groups.append(g) views.filters.remove(f)
            Group.query.filter(Group.view_id == view.id).delete()
            Filter.query.filter(Filter.view_id == view.id).delete()

        for i, o in enumerate(forms.order):
            try:
                data = form.groups.data[o.lower()]
                if not data:
                    continue

                g = Group()
                g.tag = data
                g.order = i
                view.groups.append(g)
            except KeyError:
                pass

        for i, p in enumerate(filter_.values['filters']):
            f = Filter()
            f.key = p['key']
            f.condition = p['condition']
            f.values = dict(value=p['value'], value_start=p['value_start'], value_finish=p['value_finish'])
            f.order = i
            view.filters.append(f)

        if is_new:
            db_session.add(view)

        db_session.commit()
        return redirect(url_for('.view', id=view.id))

    return render_template('views/new.html', filter=filter_, form=form, view=view)

@mod.route('/<int:id>/edit', methods=['GET', 'POST'])
def edit(id):
    return new(id)

@mod.route('/<int:id>', methods=['DELETE'])
@mod.route('/<int:id>/delete')
def delete(id):
    v = get_or_404(View, View.id==id)
    db_session.delete(v)
    db_session.commit()

    return redirect(url_for('.index'))

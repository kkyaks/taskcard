from flask.ext import wtf
from flask.ext.wtf import validators

from .models import View
from ..helpers import RadioListWidget
from ..tags.models import Tag

order = ('First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth', 'Seventh')

class GroupForm(wtf.Form):
    def __init__(self, *args, **kwargs):
        ''' Since we are converting 0 -> first, 1 -> seond and so on,
            we need to convert list(`app.views.models.Group`, `app.views.models.Group`, ...)
            to an object with attributes.
        '''

        if kwargs['obj']:
            class Dummy(object): pass # does nothing but carries attributes
            dummy = Dummy()
            for o in kwargs['obj']: # list of `app.views.models.Group` objects
                setattr(dummy, order[o.order].lower(), o.tag)

            kwargs['obj'] = dummy # overwrite the object

        kwargs['csrf_enabled'] = False

        super(GroupForm, self).__init__(*args, **kwargs)

def get_tags():
    return Tag.query.all()

def check_dups_validator(current_position):
    '''If previous siblings have the same value, raise an error'''
    def validator(form, field):
        for i in range(0, current_position):
            try:
                other = form[order[i].lower()]
                if field.data and other.data == field.data:
                    raise validators.ValidationError('This tag can be appeared once.')

            except KeyError:
                pass

    return validator

def check_empty_validator(current_position):
    '''If current field is empty and next siblings have values, raise an error'''
    def validator(form, field):
        for i in range(current_position, len(order)):
            try:
                other = form[order[i].lower()]
                if not field.data and other.data:
                    raise validators.ValidationError('Empty level is not allowed in between levels.')
            except KeyError:
                pass

    return validator

for n in range(0, 5):
    t = order[n]
    f = wtf.QuerySelectField('{0} level'.format(t),
                            validators=[check_dups_validator(n),
                                        check_empty_validator(n)],
                            query_factory=get_tags,
                            allow_blank=bool(n))

    setattr(GroupForm, t.lower(), f)

class ViewForm(wtf.Form):
    ''' A form which represents :class:`cards.models.Card` '''
    '''
    def __init__(self, data, obj, *args, **kwargs):
        super(ViewForm, self).__init__(data, obj, *args, **kwargs)

        for g in obj.groups:
            self.groups[order[g.order].lower()].process(data, g.tag.id)
    '''
    types = View.type.type.enums # from the column definition

    name = wtf.TextField('Name', validators=[validators.Required(), validators.DataRequired()])
    type = wtf.RadioField('Type', choices=[(t, unicode(t).title()) for t in types],
                                  default=types[0],
                                  widget=RadioListWidget())
    groups = wtf.FormField(GroupForm, separator='__')

from formencode import validators
from sqlalchemy import and_, or_

from ..tags.models import TagValue
from ..cards.models import Card

from .widgets import SelectWidget, NumberWidget, NumberRangeWidget
from .widgets import DateWidget, DateRangeWidget

def eq(obj, key='value'): # ==
    def func(values):
        return obj.__eq__(values[key])
    return func

def ne(obj, key='value'): # !=
    def func(values):
        return obj.__ne__(values[key])
    return func

def lt(obj, key='value'): # <
    def func(values):
        return obj.__lt__(values[key])
    return func

def gt(obj, key='value'): # >
    def func(values):
        return obj.__gt__(values[key])
    return func

def le(obj, key='value'): # <=
    def func(values):
        return obj.__le__(values[key])
    return func

def ge(obj, key='value'): # >=
    def func(values):
        return obj.__ge__(values[key])
    return func

''' Filters that are used to find the right set of cards '''
class Condition(object):

    validator = validators.String()
    _required_kwargs = ()

    def __init__(self, name, widget=None, **kwargs):
        self.name = name
        self.widget = widget
        self.kwargs = kwargs
        for r in self._required_kwargs:
            if r not in self.kwargs:
                raise ValueError('{0} is required'.format(r))

    def __json__(self):
        d = {'value': self.name}

        if self.widget:
            d['type'] = self.widget.name

        return d
    def prepare_query(self, values, query):
        return self._prepare_query(values, query)

class BooleanCondition(Condition):
    _required_kwargs = ('criterion',)

    def _prepare_query(self, values, query):
        return query.filter(self.kwargs['criterion'])

class ExcatMatchCondition(Condition):
    _required_kwargs = ('criterion',)

    def _prepare_query(self, values, query):
        criterion = self.kwargs['criterion']

        if not isinstance(criterion, list):
            criterion = [criterion]

        for c in criterion:
            query = query.filter(c(values))

        return query

class TagTypeMatchCondition(Condition):
    ''' `app.tags.models.Tag` '''
    def _prepare_query(self, values, query):
        c = Card.tag_values.any(TagValue.tag_id == values['value'])

        if 'negation' in self.kwargs and self.kwargs['negation']:
            c = ~c

        return query.filter(c)

class TagValueMatchCondition(Condition):
    ''' `app.tags.models.Tag` '''
    def _prepare_query(self, values, query):
        c = Card.tag_values.any(TagValue.id == values['value'])

        if 'negation' in self.kwargs and self.kwargs['negation']:
            c = ~c

        return query.filter(c)

class BaseFilter(object):
    ''':class:`app.cards.filters.BaseFilter` is a base class for all filters'''
    def __init__(self):
        self._dict = dict([(c.name, c) for c in self.conditions])

    def get_condition(self, name):
        return self._dict[name] if name in self._dict else None

    def prepare_query(self, values, query):
        c = self.get_condition(values['condition'])
        return c.prepare_query(values, query)

class IsComepleteFilter(BaseFilter):
    name = 'Completed'
    conditions = (BooleanCondition('is checked', criterion=Card.is_complete == True),
                  BooleanCondition('is not checked', criterion=Card.is_complete == False))

class TagValueFilter(BaseFilter):
    name = 'Tag'
    conditions = (TagValueMatchCondition('is', widget=SelectWidget(style='width:220px;')),
                  TagValueMatchCondition('is not', negation=True, widget=SelectWidget(style='width:220px;')))

class TagTypeFilter(BaseFilter):
    name = 'Tag Type'
    conditions = (TagTypeMatchCondition('is', widget=SelectWidget(style='width:140px;')),
                  TagTypeMatchCondition('is not', negation=True, widget=SelectWidget(style='width:140px;')))

class ActualHourFilter(BaseFilter):
    name = 'Actual Hour'
    conditions = (ExcatMatchCondition('is', criterion=eq(Card.actual_hours), widget=NumberWidget()),
                  ExcatMatchCondition('is not', criterion=ne(Card.actual_hours), widget=NumberWidget()),
                  ExcatMatchCondition('greater than', criterion=gt(Card.actual_hours), widget=NumberWidget()),
                  ExcatMatchCondition('less than', criterion=lt(Card.actual_hours), widget=NumberWidget()),
                  ExcatMatchCondition('in the range', criterion=[ge(Card.actual_hours, 'value_start'), le(Card.actual_hours, 'value_finish')], widget=NumberRangeWidget()),
                  BooleanCondition('is set', criterion=and_(Card.actual_hours != None, Card.actual_hours != 0)),
                  BooleanCondition('is not set', criterion=or_(Card.actual_hours == None, Card.actual_hours == 0)),
                  )

class ActualDateStartFilter(BaseFilter):
    name = 'Actual Date Start'
    conditions = (ExcatMatchCondition('is', criterion=eq(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('is not', criterion=ne(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('greater than', criterion=gt(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('less than', criterion=lt(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('in the range', criterion=[ge(Card.actual_finish, 'value_start'), le(Card.actual_finish, 'value_finish')], widget=DateRangeWidget()),
                  BooleanCondition('is set', criterion=and_(Card.actual_finish != None, Card.actual_finish != 0)),
                  BooleanCondition('is not set', criterion=or_(Card.actual_finish == None, Card.actual_finish == 0)),
                  )

class ActualDateFinishFilter(BaseFilter):
    name = 'Actual Date Finish'
    conditions = (ExcatMatchCondition('is', criterion=eq(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('is not', criterion=ne(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('greater than', criterion=gt(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('less than', criterion=lt(Card.actual_finish), widget=DateWidget()),
                  ExcatMatchCondition('in the range', criterion=[ge(Card.actual_finish, 'value_start'), le(Card.actual_finish, 'value_finish')], widget=DateRangeWidget()),
                  BooleanCondition('is set', criterion=and_(Card.actual_finish != None, Card.actual_finish != 0)),
                  BooleanCondition('is not set', criterion=or_(Card.actual_finish == None, Card.actual_finish == 0)),
                  )

class ActualDateFilter(BaseFilter):
    name = 'Actual Date'
    conditions = (Condition('is', widget=DateWidget()),
                  Condition('is not', widget=DateWidget()),
                  Condition('greater than', widget=DateWidget()),
                  Condition('less than', widget=DateWidget()),
                  Condition('in the range', widget=DateRangeWidget()))

class PlannedHourFilter(BaseFilter):
    name = 'Planned Hour'
    conditions = (ExcatMatchCondition('is', criterion=eq(Card.planned_hours), widget=NumberWidget()),
                  ExcatMatchCondition('is not', criterion=ne(Card.planned_hours), widget=NumberWidget()),
                  ExcatMatchCondition('greater than', criterion=gt(Card.planned_hours), widget=NumberWidget()),
                  ExcatMatchCondition('less than', criterion=lt(Card.planned_hours), widget=NumberWidget()),
                  ExcatMatchCondition('in the range', criterion=[ge(Card.planned_hours, 'value_start'), le(Card.planned_hours, 'value_finish')], widget=NumberRangeWidget()),
                  BooleanCondition('is set', criterion=and_(Card.planned_hours != None, Card.planned_hours != 0)),
                  BooleanCondition('is not set', criterion=or_(Card.planned_hours == None, Card.planned_hours == 0)),
                  )

class PlannedDateStartFilter(BaseFilter):
    name = 'Planned Date Start'
    conditions = (ExcatMatchCondition('is', criterion=eq(Card.planned_start), widget=DateWidget()),
                  ExcatMatchCondition('is not', criterion=ne(Card.planned_start), widget=DateWidget()),
                  ExcatMatchCondition('greater than', criterion=gt(Card.planned_start), widget=DateWidget()),
                  ExcatMatchCondition('less than', criterion=lt(Card.planned_start), widget=DateWidget()),
                  ExcatMatchCondition('in the range', criterion=[ge(Card.planned_start, 'value_start'), le(Card.planned_start, 'value_finish')], widget=DateRangeWidget()),
                  BooleanCondition('is set', criterion=and_(Card.planned_start != None, Card.planned_start != 0)),
                  BooleanCondition('is not set', criterion=or_(Card.planned_start == None, Card.planned_start == 0)),
                  )

class PlannedDateFinishFilter(BaseFilter):
    name = 'Planned Date Finish'
    conditions = (ExcatMatchCondition('is', criterion=eq(Card.planned_finish), widget=DateWidget()),
                  ExcatMatchCondition('is not', criterion=ne(Card.planned_finish), widget=DateWidget()),
                  ExcatMatchCondition('greater than', criterion=gt(Card.planned_finish), widget=DateWidget()),
                  ExcatMatchCondition('less than', criterion=lt(Card.planned_finish), widget=DateWidget()),
                  ExcatMatchCondition('in the range', criterion=[ge(Card.planned_finish, 'value_start'), le(Card.planned_finish, 'value_finish')], widget=DateRangeWidget()),
                  BooleanCondition('is set', criterion=and_(Card.planned_finish != None, Card.planned_finish != 0)),
                  BooleanCondition('is not set', criterion=or_(Card.planned_finish == None, Card.planned_finish == 0)),
                  )

class PlannedDateFilter(BaseFilter):
    name = 'Planned Date'
    conditions = (Condition('is', widget=DateWidget()),
                  Condition('is not', widget=DateWidget()),
                  Condition('greater than', widget=DateWidget()),
                  Condition('less than', widget=DateWidget()),
                  Condition('in the range', widget=DateRangeWidget()))

import json

from formencode import validators, foreach
from formencode.schema import Schema
from formencode.variabledecode import NestedVariables

from .filters import IsComepleteFilter, TagValueFilter, TagTypeFilter
from .filters import ActualHourFilter, ActualDateFilter, ActualDateStartFilter
from .filters import ActualDateFinishFilter, PlannedHourFilter
from .filters import PlannedDateFilter, PlannedDateStartFilter
from .filters import PlannedDateFinishFilter
from .widgets import SelectWidget

from ..tags.models import Tag, TagValue
from ..helpers import HTMLString, html_params

def _default(obj):
    '''
    *Jsonifies* the given object by calling :method:`__json__` and 
    :method:`__html__` of the :param:`obj`.

    Raises TypeError

    Use this with `json.dumps`

    >>> obj = HTMLString('<Hello world>')
    >>> json.dumps(obj, default:_default)
    '''

    if hasattr(obj, '__json__'):
        return obj.__json__()

    if hasattr(obj, '__html__'):
        return obj.__html__()

    raise TypeError('The given object has no __json__ nor __html__ method.')

class FilterForm(Schema):
    _filters = (IsComepleteFilter(), TagValueFilter(), TagTypeFilter(),
                ActualHourFilter(), ActualDateStartFilter(),
                ActualDateFinishFilter(), PlannedHourFilter(),
                PlannedDateStartFilter(), PlannedDateFinishFilter(),)

    pre_validators = [NestedVariables()]
    allow_extra_fields = True

    class filters(foreach.ForEach):
        class schema(Schema):
            key = validators.String(not_empty=True)
            condition = validators.String(not_empty=True)
            value = validators.String(if_missing=None, if_empty=True)
            value_start = validators.String(if_missing=None, if_empty=True)
            value_finish = validators.String(if_missing=None, if_empty=True)

    def __init__(self, args=None, allow_empty=False):
        self.args = args
        self.allow_empty = allow_empty
        self.values = self.to_python(args)
        self._dict = dict([(f.name, f) for f in self._filters])

    def get_filter(self, name):
        return self._dict[name] if name in self._dict else None

    def prepare_query(self, query):
        for values in self.values['filters']:
            f = self.get_filter(values['key'])
            query = f.prepare_query(values, query)
        return query

    def __call__(self, submit=True, **kwargs):
        ''' return html part '''
        kwargs.setdefault('class', '')
        kwargs['class'] += ' form-horizontal filters'
        html = []

        if self.allow_empty:
            html.append('<div class="control-group if-empty">'
                        '<a href="#add" class="btn">Add New Filter</a>'
                        '</div>')

        if submit:
            html.append('<div class="control-group">'
                        '<button class="btn btn-primary">Filter</button>'
                        ' <a href="?">Reset</a></div>')

        return HTMLString('<div {0}><div></div>{1}</div>'.format(html_params(**kwargs), ''.join(html)))


    def stylesheet(self):
        return HTMLString('''
.filters .control-label {
    padding-top: 0;
}
.filters .control-label .chzn-container {
    text-align: left;
}
''')

    def javascript(self):
        filters = []
        conditions = {}
        markups = {'select': SelectWidget(style='width:170px')}
        tag_keys = Tag.query.all()
        tags = TagValue.query.all()

        for f in self._filters:
            filters.append({'value': f.name})
            conditions[f.name] = f.conditions
            for c in f.conditions:
                if c.widget and c.widget.name not in markups:
                    markups[c.widget.name] = c.widget

        return HTMLString('''
    var index = 0;
    var name = '';
    var filters = {0};
    var conditions = {1};
    var markups = {2};
    var values = {3};
    var tag_keys = {4};
    var tags = {5};
    var allow_empty = {6};
    var line = '<div class="control-group"><div class="control-label">{{{{{{filters}}}}}}</div><div class="controls"></div></div>';
    var buttons = '<span class="help-inline"><a href="#add" class="btn btn-small" style="padding: 2px 6px; margin-left:10px;margin-top: -2px;"><i class="icon-plus"></i></a><a href="#remove" class="btn btn-small" style="padding: 2px 6px; margin-left:10px;margin-top: -2px;"><i class="icon-minus"></i></a></span>';

    var empty_div = $('.filters > div.if-empty');
    var filter_div = $('.filters > div:eq(0)');
    if (values.filters.length) {{
        empty_div.hide();
    }}
    filter_div.parent().on('click', 'a.btn', function(e) {{
        e.preventDefault();
        var t = $(this);

        if (t.attr('href') == '#add') {{
            empty_div.hide();
            add_line();
        }} else {{
            var p = t.parents('div.control-group');
            if (allow_empty) {{
                if (!p.siblings().length) {{
                    empty_div.show();
                }}
                p.remove();
            }} else {{
                if (p.siblings().length) {{
                    p.remove();
                }}
            }}
        }}
    }});

    function n(key) {{
        return Mustache.render('filters-{{{{i}}}}.{{{{k}}}}', {{i:index, k: key}});
    }}

    function update_values() {{
        var c = $(this);
        var t = null;
        var v = {{name: n('value')}};
        var value = c.siblings('.value');

        if (t = c.find(':selected').data('type')) {{
            if (value && value.data('type') == t) {{
                return;
            }} else {{
                value.next('.chzn-container').remove();
                value.remove();
            }}

            var f = c.parents('.control-group').find('.control-label select:eq(0)').val();
            var tag = t.split('-')[0];


            if (tag == 'select') {{
                if (f == 'Tag') {{
                    v['options'] = tags;
                }} else {{
                    v['options'] = tag_keys;
                }}
            }}

            var elem = $(Mustache.render(markups[t], v)).data('type', t).addClass('value').css('margin-left', '20px');

            if (c.data('values')) {{
                values = c.data('values');
                c.removeData('value');
                elem.filter('[name$=value]').val(values.value);
                elem.filter('[name$=value_start]').val(values.value_start);
                elem.filter('[name$=value_finish]').val(values.value_finish);
            }}

            c.siblings('span.help-inline').before(elem);

            if (elem.prop('tagName') == 'SELECT') {{
                elem.chosen().next('.chzn-container').css('margin-left', '20px');
            }}

        }} else {{
            value.remove();
        }}
    }}

    function update_conditions() {{
        var f = $(this);
        var c = conditions[f.val()];
        var next = f.parent().next().empty();

        var elem = $(Mustache.render(markups.select, {{options: c, name: n('condition')}})).addClass('conditions').appendTo(next);

        if (f.data('values')) {{
            values = f.data('values');
            f.removeData('value');
            elem.val(values.condition);
            elem.data('values', values);
        }}

        elem.on('change', update_values).chosen()
        $(buttons).appendTo(next);
        elem.trigger('change');
    }}

    function add_line(values) {{
        index ++;
        name = 'filters-' + index + '.';
        var l = $(Mustache.render(line, {{filters: Mustache.render(markups.select, {{options: filters, name: n('key')}})}}));
        var s = l.find('select');

        if (values) {{
            s.val(values.key);
            s.data('values', values);
        }}

        l.appendTo(filter_div);
        s.on('change', update_conditions).chosen().trigger('change');
    }}

    if ('filters' in values) {{
        _.each(values.filters, function(item, index, list) {{
            add_line(item);
        }});
    }}

    if (index == 0 && !allow_empty) {{
        add_line();
    }}
        '''.format(json.dumps(filters, default=_default), # 0
                   json.dumps(conditions, default=_default),
                   json.dumps(markups, default=_default),
                   json.dumps(self.values),
                   json.dumps(tag_keys, default=_default),
                   json.dumps(tags, default=_default), #5
                   'true' if self.allow_empty else 'false'))


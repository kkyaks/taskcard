import json

from sqlalchemy import Integer, String, Column, ForeignKey
import sqlalchemy.types as types

from ..database import Base

class JSONEncodedDict(types.TypeDecorator):
    """Represents an immutable structure as a json-encoded string.

    Usage::
    JSONEncodedDict(255)

    """

    impl = types.Unicode

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = unicode(json.dumps(value))
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value

class Filter(Base):
    __tablename__ = 'view_filter'

    id = Column(Integer, primary_key=True)
    view_id = Column(Integer, ForeignKey('view.id', ondelete='cascade'))
    order = Column(Integer, default=0)
    key = Column(String)
    condition = Column(String)
    values = Column(JSONEncodedDict)

    def __str__(self):
        if all([not x for x in self.values.values()]):
            value = ''
        elif self.values['value'] is not None:
            value = self.values['value']
        else:
            value = '{0[value_start]} to {0[value_finish]}'.format(self.values)

        return '{0.key} - {0.condition} {1} '.format(self, value)

from ..helpers import html_params

class BaseWidget(object):
    name = None
    attr = {}

    def __init__(self, **kwargs):
        self.attr = kwargs
        if kwargs:
            self.name += '-'+'-'.join(['{0}-{1}'.format(k, v) for k, v in kwargs.iteritems()])

        self.attr.setdefault('class', 'input-medium')

    def __html__(self):
        # escape {{ and }} since `string.format` recognises {{some text}} patterns
        markup = self.markup().replace('{{', '{{{{').replace('}}', '}}}}')
        return markup.format(html_params(**self.attr))

    def __cmp__(self, other):
        return 0 if self.markup() == other.markup() else 1

class SelectWidget(BaseWidget):
    name = 'select'
    def markup(self):
        return ('<select name="{{name}}" {0}>'
                '{{#options}}<option value="{{id}}{{^id}}{{value}}{{/id}}"'
                '{{#selected}} selected="selected"{{/selected}}'
                '{{#type}} data-type="{{type}}"{{/type}}>'
                '{{tag_value}}{{^tag_value}}{{value}}{{/tag_value}}'
                '</option>{{/options}}'
                '</select>')

class InputWidget(BaseWidget):
    name = 'text'
    def markup(self):
        return '<input name="{{name}}" type="text" {0} />'

class NumberWidget(BaseWidget):
    name = 'number'
    def markup(self):
        return '<input name="{{name}}" type="number" {0} />'

class NumberRangeWidget(BaseWidget):
    name = 'numberrange'
    def markup(self):
        return ('<input name="{{name}}_start" type="number" {0} /><span> to </span>'
                '<input name="{{name}}_finish" type="number" {0} />')

class DateWidget(BaseWidget):
    name = 'date'
    def markup(self):
        return '<input name="{{name}}" type="date" {0} />'

class DateRangeWidget(BaseWidget):
    name = 'daterange'
    def markup(self):
        return ('<input name="{{name}}_start" type="date" {0} /><span> to </span>'
                '<input name="{{name}}_finish" type="date" {0} />')


import urlparse
from urllib import urlencode

from flask import flash, session, redirect, render_template, request, url_for
import oauth2 as oauth
import trello

from . import app
from .helpers import public

@app.route('/users')
def users():
    return render_template('index.html')

@app.route('/')
def index():
    return render_template('index.html')

'''
    Trello oAuth using ugly oauth2 library
    In future, probably it's a good idea to replace it with [sanction]

    [sanction]: https://github.com/demianbrecht/sanction
'''

_oauth_urls = {
    'request_token': 'https://trello.com/1/OAuthGetRequestToken',
    'authorize': 'https://trello.com/1/OAuthAuthorizeToken',
    'access_token': 'https://trello.com/1/OAuthGetAccessToken',
}

_oauth_consumer = oauth.Consumer(app.config.get('TRELLO_API_KEY'),
                                 app.config.get('TRELLO_API_SECRET'))

@app.route('/login')
@public
def login():
    if 'access_token' in session:
        flash('You are already logged in.')
        return redirect(url_for('index'))

    client = oauth.Client(_oauth_consumer)

    body = urlencode({'oauth_callback': url_for('trello_authenicated',
                                                _external=True)})
    resp, content = client.request(_oauth_urls['request_token'],
                                   'POST', body=body)

    if resp['status'] != '200':
        raise Exception("Invalid response %s." % resp['status'])

    request_token = dict(urlparse.parse_qsl(content))
    session['request_token'] = request_token

    return redirect('{0[authorize]}?'
                    'oauth_token={1[oauth_token]}&'
                    'name=Trellest&scope=read,write'.format(_oauth_urls,
                                                            request_token))

@app.route('/logout')
def logout():
    flash('You are logged out.')
    for key in ['access_token', 'request_token', 'trello']:
        try:
            del session[key]
        except KeyError:
            pass
    return redirect(url_for('index'))

@app.route('/trello_authenicated')
@public
def trello_authenicated():
    token = oauth.Token(session['request_token']['oauth_token'],
                        session['request_token']['oauth_token_secret'])
    token.set_verifier(request.args['oauth_verifier'])
    client = oauth.Client(_oauth_consumer, token)

    resp, content = client.request(_oauth_urls['access_token'], 'GET')

    if resp['status'] != '200':
        raise Exception("Invalid response %s." % resp['status'])

    session['access_token'] = dict(urlparse.parse_qsl(content))

    client = trello.TrelloClient(app.config.get('TRELLO_API_KEY'), session['access_token']['oauth_token'])

    me = client.fetch_json('/member/me')
    session['trello'] = { 'id': me['id'],
                          'username': me['username'],
                          'full_name': me['fullName'],
                          'org_ids': me.get('idOrganizations', []),
                        }

    flash('You are logged in.')
    return redirect(url_for('index'))


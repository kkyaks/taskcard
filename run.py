from tornado.wsgi import WSGIContainer
from tornado.ioloop import IOLoop
from tornado.web import FallbackHandler, Application

from sockjs.tornado import SockJSRouter

from app import app
import config
from streaming.connection import Connection
from streaming.mqobserver import PikaObserver


if __name__ == '__main__':
    streaming_router = SockJSRouter(Connection, '.*/streaming')
    application = Application(streaming_router.urls +
                              [(r'.*', FallbackHandler, dict(fallback=WSGIContainer(app)))],
                              debug=config.DEBUG)

    io_loop = IOLoop.instance()

    # attaching observer and flask app to `streaming.connection.Connection`
    # so it can be accessible within the class
    Connection.observer = PikaObserver(io_loop)
    Connection.observer.connect()
    Connection.flask = app

    print 'Running...'
    application.listen(5000)
    io_loop.start()

'''
The queue dict stores queue declarations.

 :param queue: The queue name
 :type queue: str or unicode
 :param bool passive: Only check to see if the queue exists
 :param bool durable: Survive reboots of the broker
 :param bool exclusive: Only allow access by the current connection
 :param bool auto_delete: Delete after consumer cancels or disconnects
 :param bool nowait: Do not wait for a Queue.DeclareOk
 :param dict arguments: Custom key/value arguments for the queue

This dict feeds into `pika.channel.Channel.queue_declare`.
See also queue_declare in `pika.adapters.blocking_connection`

We have a one file to declare queues because if parameters does not match with
existing queues, RabbitMQ closes the channel with the following message:

.. code::
    <Channel.Close(['class_id=50', 'method_id=10', 'reply_code=406',
                    "reply_text=PRECONDITION_FAILED - parameters for queue
                     'hello' in vhost '/ ' not equivalent"])>'])>
'''

queues = {
    'streaming': {
        'queue': 'streaming',
        'durable': True,
        'exclusive': False,
        'auto_delete': False,
    },
    'task': {
        'queue': 'task',
        'durable': True,
        'exclusive': False,
        'auto_delete': False,
    }
}

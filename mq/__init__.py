''' Message Queue related stuffs '''
import pika
from .queues import queues
import config

def get_connection_params(url_format=False):
    credentials = (config.MQ_USERNAME, config.MQ_PASSWORD)

    params = {
        'host': config.MQ_HOST,
        'port': config.MQ_PORT,
        'virtual_host': config.MQ_VIRTUAL_HOST,
    }

    if url_format:
        # so that it can be consumed in Celery
        return ('amqp://{0[0]}:{0[1]}'
               '@{1[host]}:{1[port]}/{1[virtual_host]}').format(credentials, params)

    return pika.ConnectionParameters(credentials=pika.PlainCredentials(*credentials),
                                     **params)

class BlockingConnection:
    def __init__(self, queue):
        self.queue = queue
        self.connection = pika.BlockingConnection(get_connection_params())
        self.channel = self.connection.channel()
        self.channel.queue_declare(**queues[queue])

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.channel.close()
        self.connection.close()

    def basic_publish(self, exchange, body, *args, **kwargs):
        ''' An helper method which proxies
            `pika.adapters.blocking_connection.channel.basic_publish`
        See http://www.rabbitmq.com/amqp-0-9-1-reference.html#basic.publish
        '''

        return self.channel.basic_publish(exchange, self.queue, body,
                                          *args, **kwargs)

import time

from celery import Celery, group
from dateutil import parser
from sqlalchemy.exc import InvalidRequestError
import trello

import config
import mq
from app.cards.models import Card
from app.tags.models import Tag, TagValue
from app.database import db_session
from app.helpers import get_or_create

celery = Celery('task', backend='amqp',
                broker=mq.get_connection_params(url_format=True))

@celery.task(ignore_result=True)
def import_cards(access_token, item_id, item_type):
    ''' Import card from Trello'''
    client = trello.TrelloClient(config.TRELLO_API_KEY, access_token)
    refetch = None

    if item_type == 'organization':
        refetch = '/organization/{0}/boards'
        type_ = 'board'
    elif item_type == 'board':
        refetch = '/board/{0}/lists'
        type_ = 'list'
    elif item_type == 'list':
        refetch = '/list/{0}/cards'
        type_ = 'card'

    if refetch:
        data = client.fetch_json(refetch.format(item_id))
        # group...
        _task = import_cards.subtask
        g = group(_task((access_token, d['id'], type_)) for d in data)
        g.apply_async()
        return

    if item_type != 'card':
        raise TypeError('{0} is not supported type'.format(item_type))

    data = client.fetch_json('/card/{0}'.format(item_id))
    '''
    The following request does not work :( It's on the API doc thou...
    data = client.fetch_json('/card/{0}'.format(item_id), query_params={
            'board_fields': 'name',
            'list_fields': 'name',
        })
    '''
    board = client.fetch_json('/card/{0}/board'.format(item_id))
    list_ = client.fetch_json('/card/{0}/list'.format(item_id))

    board_tag = Tag.query.filter_by(name='Board', read_only=True).one()
    list_tag = Tag.query.filter_by(name='List', read_only=True).one()

    _, card = get_or_create(Card, trello_id=data['id'])
    card.name = data['name']
    card.description = data['desc']

    # due date is null or in iso8601
    due = data['badges']['due']
    card.planned_finish = parser.parse(due).date() if due else None

    while True:
        try:
            is_new, board_tag_value = get_or_create(TagValue,
                                                    trello_id=board['id'],
                                                    tag=board_tag)
            board_tag_value.value = board['name']

            if is_new:
                db_session.add(board_tag_value)
                db_session.commit()

            break
        except InvalidRequestError:
            print 'Expiring %s' % board_tag_value
            db_session.expire(board_tag_value)
            time.sleep(1)

    while True:
        try:
            is_new, list_tag_value = get_or_create(TagValue,
                                                   trello_id=list_['id'],
                                                   tag=list_tag)
            list_tag_value.value = list_['name']

            if is_new:
                db_session.add(list_tag_value)
                db_session.commit()

            break
        except InvalidRequestError:
            print 'Expiring %s' % list_tag_value
            db_session.expire(list_tag_value)
            time.sleep(1)

    if card.id: # existing item
        for tv in card.tag_values.filter(TagValue.trello_id != None):
            card.tag_values.remove(tv)

    card.tag_values.append(board_tag_value)
    card.tag_values.append(list_tag_value)

    db_session.add(card)
    db_session.commit()

@celery.task(ignore_result=True)
def check_for_change(access_token, card_id):
    '''Check for changes'''
    card = Card.query.filter(Card.id==card_id).one()
    try:
        import_cards(access_token, card.trello_id, 'card')
    except:
        # if everything fails, still need to hide the loader
        card.stream('sync')

@celery.task(ignore_result=True)
def send_changes(access_token, card_id):
    '''Send changes to Trello '''
    card = Card.query.filter(Card.id==card_id).one()
    client = trello.TrelloClient(config.TRELLO_API_KEY, access_token)
    client.fetch_json('/card/{0}'.format(card.trello_id),
                      http_method='PUT',
                      post_args={'name': card.name,
                                 'desc': card.description,})
